'use strict';
  const gulp = require('gulp'),
        livereload = require('gulp-livereload'),
        watch = require('gulp-watch'),
        postCss = require('gulp-postcss'),
        simpleVars = require('postcss-simple-vars'),
        nested = require('postcss-nested'),
        colorRgbaFallback = require('postcss-color-rgba-fallback'),
        opacity = require('postcss-opacity'),
        pseudoelements = require('postcss-pseudoelements'),
        vmin = require('postcss-vmin'),
        pixrem = require('pixrem'),
        willChange = require('postcss-will-change'),
        customMedia = require('postcss-custom-media'),
        mediaMinMax = require('postcss-media-minmax'),
        cssnano = require('cssnano');
  let postCssPlugins = [
    simpleVars,
    nested,
    cssnano({
      autoprefixer: {
        add:true
      },
      core: false
    }),
    colorRgbaFallback,
    opacity,
    pseudoelements,
    vmin,
    pixrem,
    willChange,
    customMedia,
    mediaMinMax
  ];
  gulp.task('styles', ()=>
    gulp.src('./src/*.css')
        .pipe(postCss(postCssPlugins))
        .pipe(gulp.dest('./css'))
  );
  var phpDir = "./*.php";
  var jsDir = "./*.js";
  var cssDir = "./*.css";

  gulp.task('default', [], function(){
    // livereload.listen();
    gulp.watch('./src/*.css',['styles']);
    // gulp.watch(cssDir,function(){
    //   gulp.src(cssDir).pipe(livereload());
    // });
    // gulp.watch(phpDir,function(){
    //   gulp.src(phpDir).pipe(livereload());
    // });
    // gulp.watch(jsDir,function(){
    //   gulp.src(jsDir).pipe(livereload());
    // });
  });
