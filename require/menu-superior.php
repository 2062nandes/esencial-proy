<header class="inicio">
  <nav class="inicio">
    <div class="nav-wrapper container">
      <a href="./" class="brand-logo">
        <?php require('require/logo.php'); ?>
      </a>
      <!-- <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="fa fa-bars fa-3x" aria-hidden="true"></i> MENÚ</a> -->
      <ul class="right hide-on-med-and-down row">
        <li><a href="./">Inicio</a></li>
        <li><a href="nosotros.php">Nosotros</a></li>
        <li><a href="contactos.php">Contactos</a></li>
      </ul>
      <ul class="side-nav row center-align" id="mobile-demo">
          <li class="logo"><?php require('require/logo-esencial.php'); ?></li>
          <li class="col s4"><a class="col s12" href="./">Inicio</a></li>
          <li class="col s4"><a class="col s12" href="nosotros.php">Nosotros</a></li>
          <li class="col s4"><a class="col s12" href="contactos.php">Contactos</a></li>
          <li class="col s12"><a class="col s12" href="insumos-para-helados.php" class="btn-menu waves-effect">INSUMOS PARA HELADOS</a></li>
          <li class="col s12"><a class="col s12" href="insumos-para-embutidos.php" class="btn-menu waves-effect">INSUMOS PARA EMBUTIDOS</a></li>
          <li class="col s12"><a class="col s12" href="insumos-en-polvo.php" class="btn-menu waves-effect">INSUMOS EN POLVO</a></li>
          <li class="col s12"><a class="col s12" href="insumos-liquidos-y-otros.php" class="btn-menu waves-effect">INSUMOS LÍQUIDOS Y OTROS</a></li>
          <li class="col s12"><a class="col s12" href="representaciones.php" class="btn-menu waves-effect"><span>REPRESENTACIONES</span></a></li>
          <li class="col s12"><a class="col s12" href="asistencia-tecnica.php" class="btn-menu waves-effect">ASISTENCIA TÉCNICA</a></li>
        </nav>
      </ul>
    </div>
  </nav>
</header>
