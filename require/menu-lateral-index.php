<a class="col s12 menu-movil button-collapse center-align waves-effect hide-on-large-only" sty href="#" data-activates="mobile-demo"><i class="fa fa-bars" aria-hidden="true"></i> MENÚ</a>
<header class="contenido col s12 m5 l3">
    <nav class="menu-principal">
       <ul>
        <li class="col s12"><a href="insumos-para-helados.php" class="btn-menu waves-effect">INSUMOS<br>PARA HELADOS</a></li>
        <li class="col s12"><a href="insumos-para-embutidos.php" class="btn-menu waves-effect">INSUMOS<br>PARA EMBUTIDOS</a></li>
        <li class="col s12"><a href="insumos-en-polvo.php" class="btn-menu waves-effect">INSUMOS<br>EN POLVO</a></li>
        <li class="col s12"><a href="insumos-liquidos-y-otros.php" class="btn-menu waves-effect">INSUMOS<br>LÍQUIDOS Y OTROS</a></li>
        <li class="col s12"><a href="representaciones.php" class="btn-menu waves-effect"><span>REPRESENTACIONES</span></a></li>
        <li class="col s12"><a href="asistencia-tecnica.php" class="btn-menu waves-effect">ASISTENCIA<br>TÉCNICA</a></li>
       </ul>
    </nav>
</header>
