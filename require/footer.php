<footer>
  <div class="container">
    <div class="fot row" style="margin-bottom:0;">
          <h3>SUCURSALES:</h3>
         <div class="col s12 m6 l3">
           <h2>SANTA CRUZ</h2>
           <p><b>Sucursal:</b> Alameda Junín Nº 203<br>
              <b><i class="fa fa-phone"></i> Telf.:</b> (591-3) 332-4244<br>
              <b><i class="fa fa-fax"></i> Fax:</b> (591-3) 332-3205<br>
              <b><i class="fa fa-envelope"></i> E-mail:</b> info@esencial.com.bo</p>
           <h4>Santa Cruz - Bolivia</h4>
         </div>
         <div class="col s12 m6 l3">
           <h2>LA PAZ</h2>
           <p><b>Sucursal:</b> C. Independencia Nº 189<br>
              <b><i class="fa fa-phone"></i> Telf.:</b> (591-2) 215-2026<br>
              <b><i class="fa fa-fax"></i> Fax:</b> (591-2) 215-2212<br>
              <b><i class="fa fa-envelope"></i> E-mail:</b> lapaz@esencial.com.bo</p>
           <h4>La Paz - Bolivia</h4>
         </div>
         <div class="col s12 m6 l3">
           <h2>COCHABAMBA</h2>
           <p><b>Sucursal:</b> Av. Villarroel Nº 2030<br>
              <b><i class="fa fa-phone"></i> Telfs.:</b> (591-4) 446-0487 / 446-0259<br>
              <b><i class="fa fa-fax"></i> Fax:</b> (591-4) 412-3462<br>
              <b><i class="fa fa-envelope"></i> E-mail:</b> cbba@esencial.com.bo</p>
           <h4>Cochabamba - Bolivia</h4>
         </div>
         <div class="col s12 m6 l3">
           <h2>TARIJA</h2>
           <p><b>Sucursal:</b> C. Cochabamba Nº 166<br>
              <b><i class="fa fa-phone"></i> Telf.:</b> (591-4) 667-5061<br>
              <!-- <b><i class="fa fa-fax"></i> Fax:</b> (591-3) 332-3205<br> -->
              <b><i class="fa fa-envelope"></i> E-mail:</b> esur@esencial.com.bo</p>
           <h4>Tarija - Bolivia</h4>
         </div>
    </div>
  <div class="row creditos" style="margin-bottom:0;"  >
    <div class="col s12 m12 l6">
      <p class="center-align">Todos los Derechos Reservados &REG; <b>Ingredientes Esenciales Bolivia </b> &COPY; <?=date("Y");?>.</p>
    </div>
    <div class="col s12 m12 l3">
       <ul class="social_icons center">
         <li><a target="_black" href="#"><i class="waves-effect waves-light fa fa-facebook"></i></a></li>
         <li><a target="_black" href="#"><i class="waves-effect waves-light fa fa-twitter"></i></a></li>
         <li><a target="_black" href="#"><i class=" waves-effect waves-light fa fa-youtube"></i></a></li>
      </ul>
    </div>
    <div class="col s12 m12 l3">
      <p class="center-align ah"><a href="//ahpublic.com" target="_blank">Diseño y Programación Ah! Publicidad</a></p>
    </div>
  </div>
  </div>
</footer>
  <script type="text/javascript" src="js/jquery.js"></script>
  <!-- <script type="text/javascript" src="js/jquery.lazyload.js"></script> -->
  <!-- <script type="text/javascript" src="js/headroom.min.js"></script> -->
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="vendor/materialize/materialize.min.js"></script>
  <!-- <script type="text/javascript" src="js/wow.min.js"></script> -->
  <script type="text/javascript" src="js/main.js"></script>
