<svg style="margin: -4% 0 -6% 0; width:100%;" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 387.3 145" style="enable-background:new 0 0 387.3 145;" xml:space="preserve">
<style type="text/css">
    .st00{fill: #505050;}
    .st10{fill: #c32929;}
</style>
            <g>
                <path class="st00" d="M126.8,52.9h-0.2h-1.8c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.2,0-0.3
        c0-0.2,0-0.3,0.1-0.4c0-0.1,0.2-0.1,0.4-0.1h1.2v-7.3h-1.2c-0.1,0-0.2,0-0.3,0s-0.1,0-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.2,0-0.3
        c0-0.2,0-0.3,0.1-0.4c0-0.1,0.2-0.1,0.4-0.1h1.8h0.2h1.7c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0-0.1,0.1-0.1,0.1s-0.2,0-0.3,0h-1.2v7.3h1.2c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0H126.8z"/>
                <path class="st00" d="M148.2,44.5L148.2,44.5v8c0,0.2,0,0.3-0.1,0.4c-0.1,0-0.2,0.1-0.4,0.1h0h-0.2c-0.1,0-0.1,0-0.2,0
        c-0.1,0-0.1,0-0.2-0.1c-0.1,0-0.1-0.1-0.2-0.2c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.2-0.3-0.5c-0.2-0.2-0.3-0.5-0.6-0.9
        c-0.2-0.3-0.5-0.7-0.7-1.1s-0.5-0.8-0.8-1.3c-0.3-0.4-0.6-0.9-0.8-1.3c-0.3-0.4-0.5-0.8-0.7-1.2v6.4c0,0.1,0,0.2,0,0.3
        c0,0.1,0,0.1-0.1,0.1c0,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.2-0.1c0,0-0.1-0.1-0.1-0.1
        c0-0.1,0-0.1,0-0.3l0-8.1h-0.7v-0.6c0.1-0.1,0.2-0.1,0.3-0.2c0.1-0.1,0.2-0.1,0.4-0.1c0.1,0,0.3-0.1,0.5-0.1c0.2,0,0.3,0,0.4,0.1
        c0.1,0.1,0.2,0.1,0.3,0.2l4.4,7v-6.7c0-0.1,0-0.2,0-0.3c0-0.1,0-0.1,0.1-0.1c0,0,0.1,0,0.2-0.1s0.2,0,0.3,0c0.1,0,0.3,0,0.3,0
        c0.1,0,0.1,0,0.2,0c0,0,0.1,0.1,0.1,0.1c0,0.1,0,0.2,0,0.3V44.5z"/>
                <path class="st00" d="M163.5,51.9c0.4,0,0.8,0,1.1-0.1s0.7-0.1,0.9-0.2v-2.7c0-0.1,0-0.2,0-0.3s0-0.1,0.1-0.1c0,0,0.1,0,0.2-0.1
        s0.2,0,0.3,0c0.1,0,0.2,0,0.3,0c0.1,0,0.1,0,0.2,0s0.1,0.1,0.1,0.1c0,0.1,0,0.1,0,0.2v3.3c0,0.1,0,0.2,0,0.3c0,0.1-0.1,0.1-0.2,0.2
        s-0.3,0.1-0.4,0.1c-0.2,0-0.4,0.1-0.7,0.1c-0.3,0.1-0.5,0.1-0.8,0.1c-0.2,0-0.5,0-0.6,0c-0.7,0-1.2-0.1-1.7-0.3
        c-0.5-0.2-0.9-0.6-1.1-1c-0.3-0.4-0.5-1-0.7-1.5c-0.2-0.6-0.3-1.2-0.2-2c0-1.1,0.2-2,0.6-2.8s1-1.4,1.7-1.7
        c0.5-0.3,1.1-0.4,1.7-0.4c0.6,0,1.2,0.1,1.8,0.3c0.2,0.1,0.3,0.1,0.4,0.1c0.1,0,0.2,0.1,0.2,0.1s0.1,0.1,0.1,0.2
        c0,0.1,0,0.2-0.1,0.3c-0.1,0.2-0.1,0.3-0.2,0.4c-0.1,0.1-0.1,0.1-0.2,0.1c-0.1,0-0.1,0-0.2,0c-0.1,0-0.2-0.1-0.4-0.1
        c-0.3-0.1-0.6-0.2-0.8-0.2c-0.2,0-0.5-0.1-0.6-0.1c-0.2,0-0.4,0-0.5,0c-0.2,0-0.4,0.1-0.6,0.2c-0.4,0.2-0.7,0.5-1,0.9
        c-0.3,0.4-0.4,0.8-0.5,1.3c-0.1,0.4-0.2,1.1-0.2,1.9c0,0.8,0.2,1.5,0.5,2.2c0.2,0.3,0.4,0.6,0.6,0.7
        C162.9,51.8,163.2,51.9,163.5,51.9z"/>
                <path class="st00" d="M183.9,49c0.1,0.3,0.3,0.6,0.4,1c0.1,0.4,0.3,0.7,0.4,1c0.1,0.3,0.2,0.6,0.3,0.8c0.1,0.2,0.2,0.4,0.2,0.4h0.7
        v0.6c0,0-0.1,0-0.3,0.1c-0.2,0.1-0.5,0.1-0.8,0.1c-0.2,0-0.3-0.1-0.5-0.1c-0.2-0.1-0.2-0.1-0.3-0.2c0-0.1-0.1-0.2-0.2-0.4
        c-0.1-0.2-0.2-0.5-0.4-0.9c-0.1-0.3-0.3-0.7-0.4-1.1c-0.2-0.4-0.3-0.7-0.4-1.1h-0.2c-0.4,0-0.7,0-0.9,0s-0.4,0-0.5-0.1
        c-0.2,0-0.4-0.2-0.6-0.3v3.7c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1-0.1,0.1s-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.3,0
        c-0.1,0-0.1,0-0.2-0.1c0,0-0.1-0.1-0.1-0.1c0-0.1,0-0.1,0-0.3v-8.1c-0.4-0.1-0.7-0.1-0.7-0.3v-0.6h4.1c0.9,0,1.6,0.2,2.1,0.6
        c0.5,0.4,0.7,1.1,0.7,1.9v0.7C185.5,48,184.9,48.7,183.9,49z M182.6,48c0.5,0,0.9-0.1,1.2-0.4s0.4-0.6,0.4-1.1v-0.5
        c0-0.5-0.1-0.9-0.4-1.1c-0.3-0.3-0.7-0.4-1.2-0.4h-2.1v3.3c0.2,0.1,0.5,0.1,0.7,0.1s0.5,0.1,0.7,0.1H182.6z"/>
                <path class="st00" d="M199.4,52.7c-0.4-0.1-0.7-0.3-0.9-0.5c-0.2-0.2-0.4-0.5-0.5-0.8s-0.2-0.6-0.2-1v-6.1v0
        c-0.4-0.1-0.7-0.1-0.7-0.3v-0.6h5.9c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-4v2.9h3.4c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3
        c0,0.1,0,0.1,0,0.2s-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0H199v1.8c0,0.3,0.1,0.5,0.2,0.7c0.1,0.2,0.2,0.3,0.4,0.4
        c0.2,0.1,0.4,0.2,0.6,0.2c0.2,0,0.5,0.1,0.8,0.1h2c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-2.3C200.2,52.9,199.7,52.8,199.4,52.7z"/>
                <path class="st00" d="M216.1,52.9c-0.1,0-0.1,0-0.2-0.1c0,0-0.1-0.1-0.1-0.1c0-0.1,0-0.2,0-0.3v-8.1c-0.4-0.1-0.7-0.1-0.7-0.3v-0.6
        h3.9c0.2,0,0.3,0,0.5,0s0.3,0,0.5,0.1c0.5,0.1,0.9,0.3,1.2,0.6c0.3,0.3,0.6,0.6,0.8,1c0.2,0.4,0.3,0.9,0.4,1.4s0.1,1,0.1,1.6
        c0,0.7-0.1,1.3-0.2,1.9c-0.1,0.6-0.3,1.1-0.6,1.5c-0.3,0.4-0.6,0.8-1.1,1c-0.5,0.2-1,0.4-1.7,0.4h-2.6
        C216.3,52.9,216.2,52.9,216.1,52.9z M217.1,51.8h1.9c0.5,0,0.9-0.1,1.2-0.3c0.3-0.2,0.6-0.5,0.7-0.8c0.2-0.3,0.3-0.7,0.4-1.2
        c0.1-0.5,0.1-0.9,0.1-1.5c0-0.4,0-0.8-0.1-1.2s-0.2-0.8-0.3-1.1s-0.4-0.6-0.7-0.8c-0.3-0.2-0.7-0.3-1.2-0.3h-2v1.1L217.1,51.8z"/>
                <path class="st00" d="M236.6,52.9h-0.2h-1.8c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.2,0-0.3
        c0-0.2,0-0.3,0.1-0.4c0-0.1,0.2-0.1,0.4-0.1h1.2v-7.3h-1.2c-0.1,0-0.2,0-0.3,0s-0.1,0-0.1-0.1c0,0,0-0.1,0-0.2c0-0.1,0-0.2,0-0.3
        c0-0.2,0-0.3,0.1-0.4c0-0.1,0.2-0.1,0.4-0.1h1.8h0.2h1.7c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0-0.1,0.1-0.1,0.1s-0.2,0-0.3,0H237v7.3h1.2c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0H236.6z"/>
                <path class="st00" d="M252.8,52.7c-0.4-0.1-0.7-0.3-0.9-0.5c-0.2-0.2-0.4-0.5-0.5-0.8s-0.2-0.6-0.2-1v-6.1v0
        c-0.4-0.1-0.7-0.1-0.7-0.3v-0.6h5.9c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-4v2.9h3.4c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3
        c0,0.1,0,0.1,0,0.2s-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-3.4v1.8c0,0.3,0.1,0.5,0.2,0.7c0.1,0.2,0.2,0.3,0.4,0.4
        c0.2,0.1,0.4,0.2,0.6,0.2c0.2,0,0.5,0.1,0.8,0.1h2c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-2.3C253.7,52.9,253.2,52.8,252.8,52.7z"/>
                <path class="st00" d="M276.1,44.5L276.1,44.5v8c0,0.2,0,0.3-0.1,0.4c-0.1,0-0.2,0.1-0.4,0.1h0h-0.2c-0.1,0-0.1,0-0.2,0
        c-0.1,0-0.1,0-0.2-0.1c-0.1,0-0.1-0.1-0.2-0.2c-0.1-0.1-0.2-0.2-0.3-0.3c-0.1-0.1-0.2-0.2-0.3-0.5c-0.2-0.2-0.3-0.5-0.6-0.9
        c-0.2-0.3-0.5-0.7-0.7-1.1s-0.5-0.8-0.8-1.3c-0.3-0.4-0.6-0.9-0.8-1.3c-0.3-0.4-0.5-0.8-0.7-1.2v6.4c0,0.1,0,0.2,0,0.3
        c0,0.1,0,0.1-0.1,0.1c0,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.2-0.1c0,0-0.1-0.1-0.1-0.1
        c0-0.1,0-0.1,0-0.3l0-8.1h-0.7v-0.6c0.1-0.1,0.2-0.1,0.3-0.2c0.1-0.1,0.2-0.1,0.4-0.1c0.1,0,0.3-0.1,0.5-0.1c0.2,0,0.3,0,0.4,0.1
        c0.1,0.1,0.2,0.1,0.3,0.2l4.4,7v-6.7c0-0.1,0-0.2,0-0.3c0-0.1,0-0.1,0.1-0.1c0,0,0.1,0,0.2-0.1s0.2,0,0.3,0c0.1,0,0.3,0,0.3,0
        c0.1,0,0.1,0,0.2,0c0,0,0.1,0.1,0.1,0.1c0,0.1,0,0.2,0,0.3V44.5z"/>
                <path class="st00" d="M288.4,44.5c-0.1,0-0.2,0-0.3,0s-0.1,0-0.1-0.1c0,0,0-0.1,0-0.2s0-0.2,0-0.3c0-0.2,0-0.3,0.1-0.4
        s0.2-0.1,0.4-0.1h5.9c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3s0,0.1,0,0.2c0,0-0.1,0.1-0.1,0.1s-0.2,0-0.3,0
        h-2.4v7.9c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1-0.1,0.1s-0.1,0-0.2,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.2,0-0.3,0c-0.1,0-0.1,0-0.2-0.1
        c0,0-0.1-0.1-0.1-0.1s0-0.1,0-0.3v-7.9H288.4z"/>
                <path class="st00" d="M308.6,52.7c-0.4-0.1-0.7-0.3-0.9-0.5c-0.2-0.2-0.4-0.5-0.5-0.8s-0.2-0.6-0.2-1v-6.1v0
        c-0.4-0.1-0.7-0.1-0.7-0.3v-0.6h5.9c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-4v2.9h3.4c0.2,0,0.4,0,0.4,0.1c0,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3
        c0,0.1,0,0.1,0,0.2s-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0h-3.4v1.8c0,0.3,0.1,0.5,0.2,0.7c0.1,0.2,0.2,0.3,0.4,0.4
        c0.2,0.1,0.4,0.2,0.6,0.2c0.2,0,0.5,0.1,0.8,0.1h2c0.2,0,0.4,0,0.4,0.1s0.1,0.2,0.1,0.4c0,0.1,0,0.2,0,0.3c0,0.1,0,0.1,0,0.2
        c0,0.1-0.1,0.1-0.1,0.1c-0.1,0-0.2,0-0.3,0H310C309.5,52.9,309,52.8,308.6,52.7z"/>
                <path class="st00" d="M327.5,44.5c-0.7-0.1-1.2,0.1-1.5,0.5c-0.3,0.4-0.4,0.8-0.3,1.3c0.1,0.5,0.7,0.9,1.8,1.1
        c1.1,0.2,1.8,0.4,2.2,0.6c0.3,0.2,0.6,0.4,0.7,0.7c0.2,0.3,0.2,0.8,0.2,1.4c0,0.3,0,0.6-0.1,0.8c0,0.2-0.1,0.4-0.1,0.6
        c-0.1,0.2-0.1,0.3-0.2,0.5c-0.1,0.1-0.2,0.3-0.4,0.4c-0.3,0.2-0.6,0.4-1.1,0.5c-0.2,0.1-0.5,0.1-0.7,0.1c-0.2,0-0.5,0-0.8,0
        c-0.3,0-0.6,0-1-0.1s-0.7-0.1-1.1-0.3c-0.2-0.1-0.3-0.1-0.4-0.1s-0.2-0.1-0.2-0.1c0,0-0.1-0.1-0.1-0.2c0-0.1,0-0.2,0.1-0.3
        c0.1-0.2,0.1-0.3,0.2-0.4c0.1-0.1,0.1-0.1,0.2-0.1c0.1,0,0.2,0,0.3,0.1c0.1,0,0.3,0.1,0.5,0.2c0.6,0.2,1.2,0.3,1.8,0.3
        c0.4,0,0.7-0.1,1-0.1c0.3-0.1,0.4-0.2,0.6-0.3c0.2-0.3,0.4-0.7,0.4-1.1s0-0.8-0.1-0.9c-0.1-0.2-0.3-0.3-0.5-0.4
        c-0.3-0.1-0.7-0.2-1.4-0.4c-0.3-0.1-0.6-0.1-0.8-0.2c-0.2-0.1-0.4-0.1-0.6-0.2c-0.3-0.1-0.6-0.3-0.8-0.5c-0.2-0.2-0.4-0.4-0.5-0.7
        c0-0.1-0.1-0.3-0.1-0.6c0-0.2,0-0.5,0-0.8c0.1-0.6,0.3-1.1,0.8-1.6c0.5-0.5,1.2-0.7,2.2-0.7c0.7,0,1.4,0.1,2.1,0.3l0.3,0.1
        c0.2,0,0.3,0.1,0.3,0.2s0,0.3-0.1,0.5c0,0.1-0.1,0.2-0.1,0.3s0,0.1-0.1,0.1c0,0-0.1,0-0.2,0c-0.1,0-0.2,0-0.4-0.1
        c-0.2-0.1-0.5-0.1-0.8-0.2C328.3,44.6,327.9,44.5,327.5,44.5z"/>
            </g>
            <g>
                <path class="st00" d="M148,83.1h-15.8c0.3,1.4,1.1,2.5,2.3,3.3c1.2,0.9,2.6,1.3,4.1,1.3c2.7,0,5.6-0.7,8.9-2.2v4.9
        c-1.2,0.6-2.6,1.1-4.2,1.6s-3.8,0.7-6.3,0.7c-2.8,0-5.1-0.5-7-1.5c-1.9-1-3.4-2.4-4.3-4.2c-1-1.8-1.4-3.8-1.4-6
        c0-2.2,0.5-4.2,1.4-5.9c1-1.7,2.4-3.1,4.3-4.1c1.9-1,4.3-1.5,7-1.5c1.8,0,3.6,0.4,5.3,1.2c1.7,0.8,3.1,2.1,4.2,3.9
        c1.1,1.8,1.6,4.1,1.6,6.9V83.1z M140.6,79.2c0-1.4-0.4-2.6-1.1-3.4c-0.7-0.9-1.7-1.3-3.1-1.3c-1.3,0-2.4,0.4-3.2,1.3
        c-0.8,0.9-1.2,2-1.2,3.4H140.6z"/>
                <path class="st00" d="M167.5,85.9c0,2.1-0.8,3.7-2.3,5s-3.7,1.9-6.7,1.9c-1.7,0-3.1-0.2-4.4-0.5c-1.3-0.3-2.7-0.7-4.2-1.3v-5.6
        c1.3,0.8,2.5,1.3,3.5,1.7c1,0.4,2.1,0.6,3.2,0.6c2,0,3-0.5,3-1.6c0-0.4-0.1-0.7-0.4-1c-0.3-0.3-0.8-0.6-1.5-1s-1.7-0.8-2.9-1.3
        c-1.5-0.6-2.7-1.5-3.7-2.6c-1-1.1-1.5-2.4-1.5-4c0-1.1,0.3-2.2,1-3.2c0.7-1,1.7-1.9,3.1-2.5c1.4-0.7,3-1,5-1c0.5,0,1.1,0,1.8,0.1
        s1.6,0.2,2.8,0.5s2.2,0.7,3.2,1.2v5.5c-0.5-0.3-0.9-0.5-1.2-0.7c-0.3-0.2-0.7-0.4-1.3-0.6c-0.5-0.2-1.1-0.4-1.9-0.6
        s-1.4-0.3-2.2-0.3c-0.9,0-1.5,0.2-1.9,0.5c-0.3,0.4-0.5,0.7-0.5,1.1c0,0.4,0.2,0.7,0.5,1c0.4,0.3,1.1,0.7,2.4,1.2
        c1.8,0.7,3.2,1.4,4.2,2.1c1.1,0.7,1.8,1.5,2.3,2.3C167.3,83.7,167.5,84.7,167.5,85.9z"/>
                <path class="st00" d="M192.9,83.1h-15.8c0.3,1.4,1.1,2.5,2.3,3.3c1.2,0.9,2.6,1.3,4.1,1.3c2.7,0,5.6-0.7,8.9-2.2v4.9
        c-1.2,0.6-2.6,1.1-4.2,1.6s-3.8,0.7-6.3,0.7c-2.8,0-5.1-0.5-7-1.5c-1.9-1-3.4-2.4-4.3-4.2c-1-1.8-1.4-3.8-1.4-6
        c0-2.2,0.5-4.2,1.4-5.9c1-1.7,2.4-3.1,4.3-4.1c1.9-1,4.3-1.5,7-1.5c1.8,0,3.6,0.4,5.3,1.2c1.7,0.8,3.1,2.1,4.2,3.9
        c1.1,1.8,1.6,4.1,1.6,6.9V83.1z M185.4,79.2c0-1.4-0.4-2.6-1.1-3.4c-0.7-0.9-1.7-1.3-3.1-1.3c-1.3,0-2.4,0.4-3.2,1.3
        c-0.8,0.9-1.2,2-1.2,3.4H185.4z"/>
                <path class="st00" d="M218.8,92.4h-7.9V80.8v-0.5c0-1.2-0.2-2.3-0.6-3.2c-0.4-0.9-1.3-1.4-2.7-1.4c-0.6,0-1.2,0.1-1.7,0.4
        c-0.5,0.2-0.9,0.6-1.3,0.9c-0.4,0.4-0.7,0.7-1,1.1c-0.3,0.4-0.5,0.7-0.8,1.1v13.2h-7.9V70h7.9v3.2c1.2-1.3,2.5-2.3,3.8-2.8
        c1.3-0.5,2.7-0.8,4.2-0.8c5.4,0,8.1,3.2,8.1,9.6V92.4z"/>
                <path class="st00" d="M240.8,91.4c-0.7,0.3-1.4,0.5-2.1,0.7c-0.7,0.2-1.6,0.4-2.5,0.5c-0.9,0.1-1.9,0.2-3,0.2
        c-2.5,0-4.5-0.4-6.2-1.2c-1.7-0.8-3-1.9-3.9-3.1c-0.9-1.3-1.6-2.5-1.9-3.8c-0.4-1.3-0.5-2.5-0.5-3.5s0.2-2.2,0.5-3.5
        c0.4-1.3,1-2.6,1.9-3.8c0.9-1.2,2.2-2.2,3.9-3.1s3.8-1.2,6.3-1.2c1.7,0,3,0.1,4.1,0.4c1,0.2,2.1,0.6,3.2,0.9v6.2
        c-2.6-0.8-4.6-1.2-6.1-1.2c-1.4,0-2.7,0.4-3.8,1.3c-1.1,0.9-1.6,2.2-1.6,4c0,1.2,0.3,2.2,0.8,3c0.5,0.8,1.2,1.4,2.1,1.8
        s1.7,0.6,2.5,0.6c0.8,0,1.7-0.1,2.7-0.4c1-0.2,2.2-0.6,3.7-1V91.4z"/>
                <path class="st00" d="M251,92.4h-7.9V70h7.9V92.4z M250.4,64.8c0,0.9-0.3,1.7-1,2.4c-0.7,0.6-1.4,1-2.4,1c-0.6,0-1.2-0.1-1.7-0.4
        c-0.5-0.3-0.9-0.7-1.2-1.2c-0.3-0.5-0.4-1.1-0.4-1.7c0-0.9,0.3-1.7,1-2.4c0.6-0.7,1.4-1,2.4-1c0.9,0,1.7,0.3,2.4,1
        C250.1,63.1,250.4,63.9,250.4,64.8z"/>
                <path class="st00" d="M277.8,91.3c-1.2,0.5-2.2,0.8-3.2,1.1c-1,0.3-2.2,0.4-3.5,0.4c-1.3,0-2.2-0.2-2.7-0.7s-0.9-1-1.1-1.7
        c-0.9,0.7-1.8,1.3-2.8,1.7c-1,0.4-2.4,0.7-4.1,0.7c-1.6,0-2.9-0.3-4-0.8s-1.7-1.3-2.1-2.1s-0.6-1.7-0.6-2.7c0-1.4,0.3-2.5,1-3.4
        s1.6-1.6,2.8-2.2c1.2-0.6,2.7-1.2,4.7-1.8c1.1-0.4,1.9-0.6,2.5-0.8c0.6-0.2,1-0.3,1.3-0.4c0.3-0.1,0.6-0.2,0.9-0.4
        c0-1.1-0.2-1.9-0.7-2.6c-0.4-0.6-1.4-0.9-2.9-0.9c-1.7,0-3.3,0.4-4.8,1.1c-1.5,0.8-2.8,1.7-4.1,3v-6.1c1-0.7,2-1.3,3.2-1.7
        c1.1-0.4,2.3-0.8,3.6-1c1.2-0.2,2.5-0.3,3.8-0.3c3.2,0,5.7,0.6,7.3,1.9c1.6,1.3,2.5,3,2.5,5.2v8.7c0,0.8,0.1,1.4,0.2,1.8
        c0.2,0.4,0.4,0.6,0.8,0.6c0.5,0,1.2-0.2,2-0.6V91.3z M266.8,87v-5.3c-1.9,0.8-3.3,1.4-4.1,2.1c-0.9,0.6-1.3,1.4-1.3,2.5
        c0,0.7,0.2,1.3,0.6,1.7c0.4,0.4,0.9,0.6,1.5,0.6c0.5,0,1.1-0.2,1.6-0.5C265.7,87.8,266.2,87.4,266.8,87z"/>
                <path class="st00" d="M286.7,92.4h-7.9V61.9h7.9V92.4z"/>
                <path class="st00" d="M313.2,83.1h-15.8c0.3,1.4,1.1,2.5,2.3,3.3c1.2,0.9,2.6,1.3,4.1,1.3c2.7,0,5.6-0.7,8.9-2.2v4.9
        c-1.2,0.6-2.6,1.1-4.2,1.6s-3.8,0.7-6.3,0.7c-2.8,0-5.1-0.5-7-1.5c-1.9-1-3.4-2.4-4.3-4.2c-1-1.8-1.4-3.8-1.4-6
        c0-2.2,0.5-4.2,1.4-5.9c1-1.7,2.4-3.1,4.3-4.1c1.9-1,4.3-1.5,7-1.5c1.8,0,3.6,0.4,5.3,1.2c1.7,0.8,3.1,2.1,4.2,3.9
        c1.1,1.8,1.6,4.1,1.6,6.9V83.1z M305.8,79.2c0-1.4-0.4-2.6-1.1-3.4c-0.7-0.9-1.7-1.3-3.1-1.3c-1.3,0-2.4,0.4-3.2,1.3
        c-0.8,0.9-1.2,2-1.2,3.4H305.8z"/>
                <path class="st00" d="M332.7,85.9c0,2.1-0.8,3.7-2.3,5s-3.7,1.9-6.7,1.9c-1.7,0-3.1-0.2-4.4-0.5c-1.3-0.3-2.7-0.7-4.2-1.3v-5.6
        c1.3,0.8,2.5,1.3,3.5,1.7c1,0.4,2.1,0.6,3.2,0.6c2,0,3-0.5,3-1.6c0-0.4-0.1-0.7-0.4-1c-0.3-0.3-0.8-0.6-1.5-1s-1.7-0.8-2.9-1.3
        c-1.5-0.6-2.7-1.5-3.7-2.6c-1-1.1-1.5-2.4-1.5-4c0-1.1,0.3-2.2,1-3.2c0.7-1,1.7-1.9,3.1-2.5c1.4-0.7,3-1,5-1c0.5,0,1.1,0,1.8,0.1
        s1.6,0.2,2.8,0.5s2.2,0.7,3.2,1.2v5.5c-0.5-0.3-0.9-0.5-1.2-0.7c-0.3-0.2-0.7-0.4-1.3-0.6c-0.5-0.2-1.1-0.4-1.9-0.6
        s-1.4-0.3-2.2-0.3c-0.9,0-1.5,0.2-1.9,0.5c-0.3,0.4-0.5,0.7-0.5,1.1c0,0.4,0.2,0.7,0.5,1c0.4,0.3,1.1,0.7,2.4,1.2
        c1.8,0.7,3.2,1.4,4.2,2.1c1.1,0.7,1.8,1.5,2.3,2.3C332.5,83.7,332.7,84.7,332.7,85.9z"/>
            </g>
            <g>
                <path class="st10" d="M107.6,74.2c-1.9-11.8-8.4-20.2-12.8-26L84.1,61.6c2.7,3.8,6.2,8.4,7.3,14.8C92.5,83.3,90.3,92,83,92
        c-1.5,0-3-0.4-4.2-1.3c1.5-0.6,2.8-1.6,4-2.7c2.9-2.9,4.1-6.9,3.4-11.2c-0.7-4.6-3.2-7.5-6-11.2l-3.7,4.4c1.8,2.6,3.6,4.7,4.1,7.8
        c0.3,1.8,0.2,4.3-1.8,6.4c-2.6,2.6-5.3,2.2-9,2.3c2.4,12,16.3,14.7,23.1,6.7c3.5-4.1,5-10.9,3.9-17.6c-0.9-5.6-3.3-10-5.8-13.7
        l3.5-4.4c11.2,16.6,9.2,31.9,2.6,39.6c-10.5,12.4-33,5.3-33-12.6c0-6.8,3.2-10.7,6.2-14.5c0,0,0,0,20.8-26.1
        c-2.7-3.5-10.5-13.5-10.9-14C64.1,50.7,62.8,52.4,59.1,57.2c-6.7,8.6-11.3,16-11.3,27.1c0,5.1,1.1,10,3.1,14.4
        c-5.5-6.2-7.7-14.6-6.2-23.7c1.6-9.9,7.1-17.5,12-23.9c4.3-5.5,0.4-0.3,20.2-25.9l-3.5-4.5c-23.6,30.5-0.7,1.1-21.1,27
        c-5.8,7.4-11.4,15.7-13.1,26.4c-1.9,11.6,1.2,22.2,8.9,29.8c9.6,9.4,22.4,9.7,24.6,9.2c-7.9-3.8-12.6-9.8-14.4-12.4
        c-6.6-9.9-6.6-23,0.1-33c4.6-6.9-0.2-0.2,21.9-29c1.3,1.7,2.8,3.6,3.8,4.9C83.7,44.1,66,66.3,66,66.3c-3.3,4.1-7.4,9.3-7.4,18
        c0,13.5,11,24.4,24.4,24.4C101.1,108.8,110.5,92.2,107.6,74.2z"/>
            </g>
</svg>
