<head>
  <meta charset="utf-8">
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
  <link rel="icon" href="/favicon.png" type="image/png">
  <meta name="designer" content="Fernando Javier Averanga Aruquipa"/>
  <meta name="description" content="Insumos para helados y embutidos. Ingredientes en polvo y en líquidos. Asistencia técnica."/>
  <meta name="author" content=""/>
  <meta name="keywords" content="COLORANTES ALIMENTICIOS, proveedores de colorantes alimenticios, Información Comercial, noticias, oportunidades de compra, empresas, colorantes alimenticios información"/>
  <title>Esencial | Ingredientes e insumos alimenticios</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="vendor/materialize/materialize.min.css"  media="screen,projection"/>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <!-- <link href="css/animate.css" rel="stylesheet"> -->
  <link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
