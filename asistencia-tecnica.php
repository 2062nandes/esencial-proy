<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Asistencia Técnica</h2>
    <article class="col s12">
      <div class="col s12 m6 l7">
        <p>Buscamos la producción de alimentos de calidad, por esto brindamos Asistencia Técnica Gratuita Permanente. Esencial incentiva y patrocina el envío a las plantas piloto de la Duas Rodas en Brasil a técnicos de las empresas Bolivianas para capacitaciones y desarrollo de productos dentro de nuestras diferentes especialidades.</p>
        <p>En Bolivia, en la matriz de Santa Cruz y brevemente en la sucursal de Cochabamba se dan Cursos de fabricación de helados a grupos de personas o entrenamiento individual, a solo requerimiento del cliente. Sin costo sin condicionamientos ni obligación de reciprocidad.</p>
      </div>
      <div class="col s12 m6 l5">
        <picture>
          <source srcset="images\asistencia-tecnica.jpg" media="(max-width:660px)">
          <source srcset="images\asistencia-tecnica.jpg" media="(min-width:660px)">
          <img style="width:100%;" class="responsive-img materialboxed" src="images\asistencia-tecnica.jpg" alt="insumos liquidos y otros">
        </picture>
      </div>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
