<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Insumos para Embutidos</h2>
    <article class="col s12">
      <picture>
        <source srcset="images\insumos-para-embutidos-s.jpg" media="(max-width:660px)">
        <source srcset="images\insumos-para-embutidos.jpg" media="(min-width:660px)">
        <img class="responsive-img materialboxed" src="images\insumos-para-embutidos.jpg" alt="insumos para embutidos">
      </picture>
    </article>
    <article class="col s12">
      <h3>ADITIVOS</h3>
      <p><b>Curas</b><br>
         Ingredientes fundamentales en el proceso de cura confieren al producto el desarrollo de color, sabor e olor característicos del curado, generan substancias inhibidoras de microorganismos, especialmente contra o Clostridium botulinum.
      </p>
      <p><b>Fijadores de Color</b><br>
         Poseen función antioxidante que, en conjunto con la cura, proporcionan al producto mayor tiempo de vida (shelf-life), actuando directamente en el color y la conservación.
      </p>
      <p><b>Estabilizantes</b><br>
         Aumentan la retención de agua e grasa del producto, regulando el pH, reducen las quiebras de cocimiento, proporcionan una emulsión mas estable, mejorando a suavidad e a suculencia del produjo final.
      </p>
      <p><b>Antioxidantes para Grasas</b><br>
         La función específica del antioxidante es retardar o impedir la rancidez de los alimentos, específicamente los aceites y grasas.
      </p>
      <p><b>Emulsificante</b><br>
         Es un mejorador de emulsiones, proporcionando varios efectos en diferentes productos, tales como:
      </p>
      <ul>
        <li><i class="fa fa-caret-right"></i> Proporciona mas liga a los chorizos frescos, evitando o desmigamiento al fritar o asar;</li>
        <li><i class="fa fa-caret-right"></i> Facilita a retirada da tripa do producto, evitando a adherencia;</li>
        <li><i class="fa fa-caret-right"></i> Mayor seguridad y uniformidad de producción de curados (fermentados), en salas no climatizadas;</li>
        <li><i class="fa fa-caret-right"></i> Ayuda el rebanado de productos, notadamente mortadelas y salchichones, ayudando, inclusive, a mantener el color atrayente del punto donde a pieza fue cortada.</li>
      </ul>
    </article>
    <article class="col s12">
      <h3>CONDIMENTOS</h3>
      <p>Son ingredientes o constituyentes que poseen, por si solo o en combinación con otros, desarrollar sabor y aroma en los productos cárnicos, confiriéndoles sabor característico. Son utilizadas especias, aceites esenciales y oleorresinas, casi en su totalidad derivadas de vegetales de diferentes partes de plantas. Con la aplicación de los condimentos Duas Rodas, se obtienen la standardización del sabor y aroma, evitando o trabajo de triturado optimizando espacio físico y, principalmente, proporcionando una reducción acentuada de la posibilidad de contaminación microbiológica.</p>
      <p>Condimentos para aplicaciones: Chorizos frescos, Chorizos ahumados, salchichas, mortadelas, salame, jamón, chorizo tipo calabreza, patés.</p>
    </article>
    <article class="col s12">
      <h3>OTROS PRODUCTOS</h3>
      <ul>
        <li><i class="fa fa-caret-right"></i> Aroma de ajo en polvo</li>
        <li><i class="fa fa-caret-right"></i> Aroma de pimienta roja</li>
        <li><i class="fa fa-caret-right"></i> Aroma de humo</li>
        <li><i class="fa fa-caret-right"></i> Proteínas texturizadas de soya</li>
      </ul>
    </article>
    <article class="col s12">
      <h3>TRIPAS PARA SALCHICHAS</h3>
      <ul>
        <li><i class="fa fa-caret-right"></i> Tripas de celulosa formadores de salchicha</li>
        <li><i class="fa fa-caret-right"></i> Tripas fibrosas para salames</li>
      </ul>
    </article>
    <article class="col s12">
      <h3>TRIPAS PARA MORTADELA</h3>
    </article>
    <article class="col s12">
      <h3>EMBALAJES AL VACÍO</h3>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
