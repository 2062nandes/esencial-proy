<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Contáctenos</h2>
    <article class="col s12">
      <form style="padding: 21px 4% 20px 1%;" method="post" id="theForm" class="second" action="mail.php" role="form">
        <div class="col s12 m6 l6">
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">account_circle</i>
              <input type="text" id="nombre" class="validate" name="nombre" tabindex="1" required>
              <label for="nombre">Nombre completo:</label>
            </div>
          </div>
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">phone</i>
              <input type="text" id="telefono" class="validate" name="telefono" tabindex="2" required>
              <label for="telefono">Teléfono:</label>
            </div>
          </div>
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">settings_cell</i>
              <input type="text" id="movil" class="validate" name="movil" tabindex="3" required>
              <label for="movil">Teléfono móvil:</label>
            </div>
          </div>
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">location_on</i>
              <input type="text" id="direccion" class="validate" name="direccion" tabindex="4" required>
              <label for="direccion">Dirección:</label>
            </div>
          </div>
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">location_city</i>
              <input type="text" id="ciudad" class="validate" name="ciudad" tabindex="5" required>
              <label for="ciudad">Ciudad:</label>
            </div>
          </div>
        </div>
        <div class="col s12 m6 l6">
          <div class="form_row">
            <div class="input input-field">
              <i class="material-icons prefix">email</i>
              <label for="email">Su E-mail:</label>
              <input type="email" id="email" class="validate" name="email" tabindex="6" required>
            </div>
          </div>
          <div class="form_row mensaje">
            <div class="input input-field">
              <i class="material-icons prefix">mode_edit</i>
              <label for="mensaje">Mensaje:</label>
              <textarea id="mensaje" class="materialize-textarea validate" cols="55" rows="7" name="mensaje" tabindex="7" required></textarea>
            </div>
          </div>
          <div class="form_row botones center-align">
            <i style="background-color: #0d47a1;" class="submitbtn waves-effect waves-yellow btn z-depth-3 waves-input-wrapper" style=""><input class="waves-button-input" type="submit" tabindex="8" value="Enviar"></i>
            <!-- <input class="submitbtn waves-effect waves-red" type="submit" tabindex="8" value="Enviar"> </input> -->
            <!-- <input class="deletebtn waves-effect waves-yellow btn z-depth-3" type="reset" tabindex="9" value="Borrar"> </input> -->
          </div>
          <div class="col s12">
            <div id="statusMessage"></div>
          </div>
        </div>
      </form>
    </article>
    <h2 class="center-align">Ubíquenos</h2>
    <article class="col s12">
      <!-- <object width="100%" height="480" type="" data="sucursales/"></object> -->
      <iframe src="https://www.google.com/maps/d/embed?mid=128kOUGKM0LbTOmm2EjNmVWRtDn0" width="100%" height="480"></iframe>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
