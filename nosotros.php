<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Nosotros</h2>
    <article class="col s12">
        <p><b>Una Adquisición Inteligente</b><br>
            Buscamos la producción de alimentos de calidad, por esto brindamos Asistencia Técnica Gratuita Permanente.<br>
            Esencial incentiva y patrocina el envío a las plantas piloto de la Duas Rodas en Brasil a técnicos de las empresas Bolivianas para capacitaciones y desarrollo de productos dentro de nuestras diferentes especialidades.</p>
    </article>
    <article class="col s12">
      <?php require('require/logo-esencial.php'); ?>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
