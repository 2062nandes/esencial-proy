<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Insumos para Helados</h2>
    <article class="col s12">
      <picture>
        <source srcset="images\insumos-para-helados-s.jpg" media="(max-width:660px)">
        <source srcset="images\insumos-para-helados.jpg" media="(min-width:660px)">
        <img class="responsive-img materialboxed" src="images\insumos-para-helados.jpg" alt="">
      </picture>
    </article>
    <article class="col s12">
      <h3>ALGEMIX</h3>
      <p><b>Sabores:</b> Piña , Ciruela negra, Banana, Blue Ice, Bombón New, Chicle, Chocolate Blanco, Coco, Crema Caramelo, Dulce de Leche, Dulce de leche Gold, Leche Condensada, Papaya, Mango, Maracuyá, Menta, Mora, Frutilla , Frutilla Premium, , Crema Americana (Nata), Nueces, Pasas al Ron, Durazno (Melocotón), Pistacho, Piña Colada, Cuatro Leches, Leche Condensada, Uva, Vainilla, <br>
         *Guayaba*. *, Brigadeiro*, Cereza*, Chocante*, Chocolate*, Chocolate Suízo*, Frutillagurt*, Yogur*, Kiwi*.</p>
      <p><b>Dosificación recomendada:</b> 20g para cada litro de leche<br>
         *Utilizar 50g para cada litro de agua o leche.</p>
      <p><b>Embalajes disponibles:</b> bolsas plásticas de 1kg ó 800g y bolsas multicapas a granel de 25kg, dependiendo del sabor.</p>
    </article>
    <article class="col s12">
      <h3>SELECTA TROPICAL</h3>
      <p>La novedad con sabor a fruta fresca es un polvo saborizante desarrollado especialmente para la elaboración de palitos a base de agua con sabores irresistibles.</p>
      <p>Selecta Tropical le confiere al palito aquel sabor a fruta fresca, recién cosechada. refrescante y natural, con sabor característico a las frutas.</p>
      <p><b>Sabores:</b> Frutilla (Fresa), Limón, Uva, Manzana Verde, Mandarina,.Piña, Limón Tahiti, Tutti-frutti, Grosella, Maracuyá.<br>
         <b>Embalaje:</b> Bolsas de 1kg.<br>
         <b>Dosificación:</b> 20g por litro de agua.
      </p>
      <p>Otros sabores en polvo</p>
      <ul>
        <li><i class="fa fa-caret-right"></i> Selecta plus Café</li>
        <li><i class="fa fa-caret-right"></i> Selecta pinta lengua azul</li>
        <li><i class="fa fa-caret-right"></i> Selecta pinta lengua rojo</li>
      </ul>
    </article>
    <h2 class="center-align">Saborizantes líquidos</h2>
    <article class="col s12">
      <h3>Emultina Selecta</h3>
      <p>Indicado también para la elaboración de helados Diet/Light, por no contener azúcar en su formulación;<br>
         Puede ser utilizado en la fabricación de otras golosinas congeladas, pasteles, y otras.</p>
      <p><b>Sabores:</b> Piña, Coco, Grosella, Naranja, Limón, Menta, Frutilla, Mandarina, Vainilla, Uva.<br>
         <b>Dosificación recomendada:</b> 2ml para cada litro de leche/agua.
      </p>
      <p>*Disponible también en frascos de vidrio de 1kg.<br>
         <b>Nota:</b> Para los sabores frutales se puede añadir ácido cítrico.</p>
    </article>
    <article class="col s12">
      <h3>Emulsionantes</h3>
      <ul>
        <li><i class="fa fa-caret-right"></i> Emustab</li>
        <li><i class="fa fa-caret-right"></i> Emustab DR</li>
      </ul>
      <p>Mejoran la emulsión y homogeneización de la mezcla base del helado, proporcionando al producto final más cremosidad, estabilidad en el almacenamiento y mejor rendimiento.</p>
      <p>Los emulsionantes Duas Rodas son los más nobles del mercado. La combinación de las materias primas seleccionadas garantiza un producto ideal para la incorporación del aire en la cantidad cierta.</p>
    </article>
    <article class="col s12">
      <h3>Estabilizantes</h3>
      <ul>
        <li><i class="fa fa-caret-right"></i> Super Liga Neutra</li>
        <li><i class="fa fa-caret-right"></i> Liga Neutra Extra Industrial</li>
        <li><i class="fa fa-caret-right"></i> Liga Neutra Extra 2</li>
      </ul>
      <p>Proporcionan homogeneidad, estabilidad, mejor textura a las mezclas, principalmente palitos de frutas, dejándolos más blandos y retardando el derretimiento.</p>
      <p><b>Aplicación:</b> Helados y palitos a base de leche o agua.</p>
    </article>
    <article class="col s12">
      <h3>Salsas Coberturas Para Copas</h3>
      <p>Para ser utilizado en la decoración de copas, tortas, y en la fabricación de helados mezclados y rellenos de palitos. Acompaña el helado en todas sus formas de consumo, enriqueciendo su sabor y su apariencia. En algunos sabores, es elaborado con pulpa de frutas.</p>
      <p>
        <b>Sabores:</b> Vainilla, Caramelo, Chocolate, Leche de Coco, Frutilla (Fresa).<br>
        <b>Sabores con pulpa:</b> Mora, Brigadeiro, Maracuyá, y Frutilla (Fresa).<br>
        <b>Embalaje disponible:</b> Tubos plásticos de 1,3kg.<br>
      </p>
    </article>
    <article class="col s12">
      <h3>Otros Productos para Helados</h3>
      <ul>
        <div class="col s12 m6 l6">
          <li><i class="fa fa-caret-right"></i> Chocolate granulado</li>
          <li><i class="fa fa-caret-right"></i> Granulado de colores</li>
          <li><i class="fa fa-caret-right"></i> Chocolate para cobertura de picole. </li>
          <li><i class="fa fa-caret-right"></i> Chocolate semi amargo en barra e industrial </li>
          <li><i class="fa fa-caret-right"></i> Chocolate a la leche en barra e industrial </li>
        </div>
        <div class="col s12 m6 l6">
          <li><i class="fa fa-caret-right"></i> Chocolate blanco en barra </li>
          <li><i class="fa fa-caret-right"></i> Gotas de chocolates </li>
          <li><i class="fa fa-caret-right"></i> Extracto de Malta </li>
          <li><i class="fa fa-caret-right"></i> Leche en polvo</li>
          <li><i class="fa fa-caret-right"></i> Ácido Cítrico</li>
        </div>
      </ul>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
