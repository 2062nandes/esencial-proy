jQuery(function($) {'use strict',
	//Initiat WOW JS
	// new WOW().init();
	$(document).ready($(function() {
		    // $("img").lazyload({
				// 	threshold : 150,
    		// 	effect : "fadeIn"
				// });
				$(".button-collapse").sideNav();
				$('.carousel.carousel-slider').carousel({fullWidth: true});
				// window.setInterval(function(){$('.carousel.carousel-slider').carousel('next')},4000)
  }));
	/*Formulario de contacto*/
  $(function(){
    $('input, textarea').each(function() {
      $(this).on('focus', function() {
        $(this).parent('.input').addClass('active');
     });
    if($(this).val() != '') $(this).parent('.input').addClass('active');
    });
  });

  var message = $('#statusMessage');
  $('.deletebtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "none");
    $("#theForm textarea").css("box-shadow", "none");
    $("#theForm select").css("box-shadow", "none");
    $(".input").removeClass("active");
  });
  $('.submitbtn').click(function() {
    message.removeClass("animMessage");
    $("#theForm input").css("box-shadow", "");
    $("#theForm textarea").css("box-shadow", "");
    $("#theForm select").css("box-shadow", "");
    if($("#theForm")[0].checkValidity()){
      $.post("mail.php", $("#theForm").serialize(), function(response) {
        if (response == "enviado"){
          $("#theForm")[0].reset();
          $(".input").removeClass("active");
          message.html("Su mensaje fue envíado correctamente,<br>le responderemos en breve");
        }
        else{
          message.html("Su mensaje no pudo ser envíado");
        }
        message.addClass("animMessage");
        $("#theForm input").css("box-shadow", "none");
        $("#theForm textarea").css("box-shadow", "none");
        $("#theForm select").css("box-shadow", "none");
      });
      return false;
    }
  });

});
