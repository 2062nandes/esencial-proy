<!DOCTYPE html>
<html lang="es">
 <?php require('require/header.php'); ?>
<body>
  <object type="text/html" data="slider/background.html" style="height:100vh; width:100%; overflow:hidden;position:fixed;z-index: -1;"></object>
  <?php require('require/menu-superior.php'); ?>
<div class="container">
  <div class="row">
    <?php require('require/menu-lateral.php'); ?>
  <section class="col s12 m12 l9">
    <h2 class="center-align">Insumos en Polvo</h2>
    <article class="col s12">
      <picture>
        <source srcset="images\insumos-en-polvo-s.jpg" media="(max-width:660px)">
        <source srcset="images\insumos-en-polvo.jpg" media="(min-width:660px)">
        <img class="responsive-img materialboxed" src="images\insumos-en-polvo.jpg" alt="insumos en polvo">
      </picture>
    </article>
    <article class="col s12">
      <h3></h3>
    </article>
  </section>
  </div>
</div>
 <?php require('require/footer.php'); ?>
</body>
</html>
